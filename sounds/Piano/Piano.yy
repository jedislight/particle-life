{
  "compression": 0,
  "volume": 0.19,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "Piano.wav",
  "duration": 1.540499,
  "parent": {
    "name": "particle-life",
    "path": "particle-life.yyp",
  },
  "resourceVersion": "1.0",
  "name": "Piano",
  "tags": [],
  "resourceType": "GMSound",
}