/// @description Insert description here
// You can write your code in this editor
var text = ""
text += "\n(G)ravity: " + string(gravity_enabled);
text += "\n(C)olor Tails: " + string(not clear_background);
text += "\n(W)rap: " + string(wrap);
text += "\n(N)earest Only: " + string(update_nearest_only);
text += "\n(M)usic: " + string(music);
text += "\n(V)isualize Music: " + string(music_visuals);
text += "\nFPS: " + string(fps);
text += "\nLimit: " + string(floor(fps_real));
text += "\nCount: " + string(instance_number(Particle));
text += "\nTypes: " + string(type_count);
draw_text(0,0, text);

var top = view_hport[0]-15;
var right = view_wport[0]-5;
var left = 5;
var bottom = top + 5;
var total = instance_number(Particle) + instance_number(Seed);
var cursor = left;
var span = right - left;
for(var i = 0; i < type_count; ++i){
	var count = 0;
	with(Particle) {if (type == i){ ++count}};
	with(Seed){ if (type == i){ ++count}};
	
	if (count == 0) continue;
	
	var percent = count / total;
	var this_span = span * percent;
	var color = type_colors[i];
	draw_rectangle_color(cursor, top, cursor+this_span, bottom,color, color ,color ,color , false );
	cursor += this_span;
}
draw_rectangle_color(left, top, right, bottom, c_white, c_white, c_white,c_white, true );

