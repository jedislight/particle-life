/// @description Insert description here
// You can write your code in this editor
randomize();

type_count = irandom_range(6,200);
type_colors = [];
repeat(type_count)
	array_push(type_colors, make_color_hsv(random(255), random_range(128,255), random_range(128, 255)));
update_limit = 1000;
update_cursor = 0;

a = ds_grid_create(type_count, type_count)
b = ds_grid_create(type_count, type_count)
c = ds_grid_create(type_count, type_count)

ae = ds_grid_create(type_count, type_count)
be = ds_grid_create(type_count, type_count)
ce = ds_grid_create(type_count, type_count)

l = ds_grid_create(type_count, type_count);

s = [];
repeat(type_count)
	array_push(s, irandom_range(1,2));

for(var xx = 0; xx < type_count; ++xx) for (var yy = 0; yy < type_count; ++yy){
	ds_grid_set(a, xx, yy, ((xx + yy) / type_count) - 1)
	ds_grid_set(b, xx, yy, ((xx + yy) / type_count) - 1)
	ds_grid_set(c, xx, yy, ((xx + yy) / type_count) - 1)
	
	ds_grid_set(ae, xx, yy, abs(((xx + yy) / type_count) - 1))
	ds_grid_set(be, xx, yy, abs(((xx + yy) / type_count) - 1))
	ds_grid_set(ce, xx, yy, abs(((xx + yy) / type_count) - 1))
	ds_grid_set(l, xx, yy, choose(true, false));
}

ds_grid_shuffle(a);
ds_grid_shuffle(b);
ds_grid_shuffle(c);

ds_grid_shuffle(ae);
ds_grid_shuffle(be);
ds_grid_shuffle(ce);

repeat(1){
	var centerish = 0 + irandom_range(-2000, 2000)
	var middleish = 0 + irandom_range(-2000, 2000);
	var p = instance_create_depth(centerish, middleish, 0, Particle)
	p.type = irandom_range(0, type_count-1);
}

music_cursor = 0;
music_temp_steps = irandom_range(20,60);
music_tempo_counter = 0;
music_right = 0;
music_left = 0;