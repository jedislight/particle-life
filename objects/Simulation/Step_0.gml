/// @description Insert description here
// You can write your code in this editor
if (music) {
	music_tempo_counter += 1
	if (music_tempo_counter >= music_temp_steps) {
		music_tempo_counter = 0;
		var camera = view_get_camera(0)
		var width = camera_get_view_width(camera) / 12;
		music_left = music_cursor + camera_get_view_x(camera);
		music_right = music_left + width;
		var middle = camera_get_view_height(camera) / 2 + camera_get_view_y(camera);
		with(Particle){
			if (x > Simulation.music_left and x < Simulation.music_right){
				var offset = (middle - y) / (camera_get_view_height(camera));
				offset = power(abs(offset), 0.25) * sign(offset);
				var pitch = 1.0 + offset;
				pitch *= 10;
				pitch = round(pitch);
				pitch /= 10;
				pitch -= 0.25;
				var note = audio_play_sound(Piano,0,false);
				audio_sound_pitch(note, pitch);
				if(Simulation.music_visuals){
					var f = instance_create_depth(x,y,depth+1, Focus);
					f.target = id;
				}
			}
		}
		music_cursor += width;	
		if (music_cursor >= camera_get_view_width(camera)) music_cursor = 0;
	}
}
if (fps < 55) {
	update_limit -= 1;	
} else {
	update_limit += 1;
}

update_limit = clamp(update_limit, 1, instance_number(Particle));

repeat(update_limit){
	if (update_cursor >= instance_number(Particle)) update_cursor = 0;
	var p = instance_find(Particle, update_cursor++);
	if (not instance_exists(p)) continue;
	var pn = Particle;
	if (update_nearest_only){
		with(p){ 
			instance_deactivate_object(id)
			pn = instance_nearest(x, y, Particle);
			instance_activate_object(id);
		}
	}
	with(pn){
		if (p.id != id){
			var px = p.x;
			var py = p.y;
			if (ds_grid_get(Simulation.l, p.type, type)){
				px += lengthdir_x(p.speed*room_speed*0.5, p.direction)	
				py += lengthdir_y(p.speed*room_speed*0.5, p.direction)	
			}
			var distance = point_distance(x,y, p.x, p.y);
			var dir = point_direction(p.x, p.y, x, y);
		
			if (distance < 32) {
				with(p) motion_add(dir, -32+distance);	
			} else {
				var a = ds_grid_get(Simulation.a, p.type, type); 
				var b = ds_grid_get(Simulation.b, p.type, type);
				var c = ds_grid_get(Simulation.c, p.type, type);
		
				var ae = ds_grid_get(Simulation.ae, p.type, type);
				var be = ds_grid_get(Simulation.be, p.type, type);
				var ce = ds_grid_get(Simulation.ce, p.type, type);
		
				var d = distance / 1024;
				var A = a * power(d, ae);
				var B = b * power(d, be);
				var C = c * power(d, ce);
				var impulse = (A + B + C) / d; 
				
				with(p) motion_add(dir, impulse);
			}
		}
	}
}

if (irandom_range(0,instance_number(Particle)) > fps_real) {
	instance_destroy(instance_find(Particle, 0));	
} 

if (random_range(0.0, instance_number(Particle)) < 0.1){
	var centerish = 0 + irandom_range(-2000, 2000)
	var middleish = 0 + irandom_range(-2000, 2000);
	var p = instance_create_depth(centerish, middleish, 0, Particle)
	p.type = irandom_range(0, type_count-1);	
}