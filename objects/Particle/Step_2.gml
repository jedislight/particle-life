/// @description Insert description here
// You can write your code in this editor
/*
if (x < 0 ) x += room_width;
if (y < 0 ) y += room_height;
if (x > room_width) x -= room_width;
if (y > room_height) y -= room_height;
*/
speed *= 0.9;
speed = clamp(speed, 0, 5);

if (collisions >= 6) {
	instance_destroy(id);	
} else if (collisions == Simulation.s[type]) {
	repeat(irandom_range(1,2)) {
		var p = instance_create_depth(x+random_range(-32,32),y+random_range(-32,32), depth, Seed);
		p.type = round(new_type);
		p.direction = direction + random_range(-10, 10);
		p.speed = speed + random_range(-0.5, 0.5);
		p.alarm[0] = irandom_range(room_speed * 0.5, room_speed *3);
	}
	instance_destroy(id);
}

if(Simulation.gravity_enabled){
	c_dir = point_direction(x, y, 0, 0);
	c_dist = point_distance(x, y, 0, 0);
	motion_add(c_dir, power(c_dist/500, 1.8));
}



if (Simulation.wrap){
	var camera = view_get_camera(0)	
	var left = camera_get_view_x(camera)
	var top = camera_get_view_y(camera);
	var width = camera_get_view_width(camera);
	var height = camera_get_view_height(camera);
	
	if (x < left) x += width;
	if (y < top) y += height;
	if (x > left +width) x -= width;
	if (y > top + height) y -= height;
}